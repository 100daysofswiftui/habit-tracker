# Habit Tracker

Habit Tracker is a habit-tracking app, for folks who want to keep track of how much they do certain things. That might be learning a language, practicing an instrument, exercising, or whatever – they get to decide which activities they add, and track it however they want.

This app was developed as a challenge from the [100 Days of SwiftUI](https://www.hackingwithswift.com/guide/ios-swiftui/4/3/challenge) course from Hacking with Swift.