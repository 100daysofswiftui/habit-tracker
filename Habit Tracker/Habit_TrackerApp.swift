//
//  Habit_TrackerApp.swift
//  Habit Tracker
//
//  Created by Pascal Hintze on 13.11.2023.
//

import SwiftUI

@main
struct Habit_TrackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
